# This Python file uses the following encoding: utf-8
import json
import socket
import random
import time

# temperature interval to simulate
tempmin=14
tempmax=35
# humidity interval to simulate
hummin=30
hummax=60
# seconds between data is sent
interval=10
# server
host='localhost'
port=4242

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host,port))
s.settimeout(1)

# send data and see if server sends anything back
def ts(str):
    s.send(str.encode())
    data = ''
    try:
        data = s.recv(1024).decode()
    except socket.timeout:
        pass
    print (data)

# my unit has an ID, change this if you want the same ID each time
unitid = random.randrange(1,100)

for i in range(1,100):
    temp=random.uniform(tempmin, tempmax)
    hum=random.uniform(hummin, hummax)
    str  =  '"unit": "{0}", "temp": "{1}", "humid": "{2}"'.format(unitid,temp,hum)
    str = "{" + str + "}" + chr(0x1E)
    ts(str)
    time.sleep(interval)
