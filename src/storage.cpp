/**
  @file storage.cpp
  @brief Database connection
  @author Peter
  @date 2019-10-06
*/

#include "storage.h"

static const QString inifilename = "/opt/pj/pjserver.ini";
static const QString testdatabase = "/opt/pj/pjdata.db";

Storage::Storage(QObject *parent) : QObject(parent)
{
    bool testing;
    QSettings settings(inifilename, QSettings::IniFormat);
    settings.beginGroup("database");
    QString hostname = settings.value("hostname").toString();
    QString username = settings.value("username").toString();
    QString password = settings.value("password").toString();
    QString databasename = settings.value("databasename").toString();
    table = settings.value("tablename").toString();
    settings.endGroup();
    settings.beginGroup("debug");
    testing = settings.value("test", 0).toBool();
    if (not testing) {
        db = QSqlDatabase::addDatabase("QMYSQL");
        db.setHostName(hostname);
        db.setUserName(username);
        db.setPassword(password);
        db.setDatabaseName(databasename);
    } else {
        db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName(testdatabase);
    }
    db.open();
    if (not db.isOpen()) {
        qCritical() << "Could not open database";
    }
}

Storage::~Storage()
{
    db.close();
}

void Storage::dataReady(QJsonObject msg)
{
    qint64 time = QDateTime::currentSecsSinceEpoch();
    QSqlQuery qry;
    QString command;
    bool result;
    QVariantMap map = msg.toVariantMap();
    map.insert("timestamp", QString::number(time));
    command = "INSERT INTO " + table + "( ";
    for (QString &id: map.keys()) {
        command += id + ",";
    }
    command.chop(1); // remove the last comma
    command += ") VALUES( ";
    for (QString &id: map.keys()) {
        command += map.value(id).toString() + ",";
    }
    command.chop(1); // remove last comma
    command += ");";
    result = qry.exec(command);
    if (not result) {
        qCritical() << "Could not execute SQL" << command;
        qCritical() << "Error code " << qry.lastError();
    }
}
