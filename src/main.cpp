/**
  @file main.cpp
  @brief Server program for IoT temperature sensors
  @author Peter
  @date 2019-10-06
  This is the main program, it sets up a socket server and a database connection.
*/

#include <QCoreApplication>
#include <QtGlobal>
#include <sys/file.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "tcpserver.h"
#include "storage.h"

/** file name for debug log. not used for release version */
QString const debuglogfile = "/opt/pj/log/pjdebug.log";
/** file name for error/info log */
QString const errorlogfile = "/opt/pj/log/pjerror.log";
/** max log size 1MB */
qint64 const maxlogsize = 1*1024*1024;
/** pid file for preventing more than one instance */
static const char *pidname = "/tmp/jamesserver.pid";
/** pid file handle */
int pid_file;

/**
 * @brief RotateAndLog checks if size of log file is exceeded, rotates, and writes to log
 * @param name filename to write
 * @param logline message to log - should not contain newlines
 * @note uses @ref maxlogsize for file size
 */
void RotateAndLog(QString const &name, QString const &logline)
{
    qint64 fsize;
    QFile outFile(name);
    if (outFile.exists()) {
        fsize = outFile.size();
    } else {
        fsize = 0;
    }
    if (fsize >= maxlogsize) {
        QString oldName = name+".1";
        QFile oldFile(oldName);
        if (oldFile.exists()) {
            oldFile.remove();
        }
        outFile.rename(oldName);
        outFile.close();
        outFile.setFileName(name);
    }
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    ts << logline << endl;
}

/**
 * @brief customMessageHandler installable message handler for Qt debug and log function
 * @param type debug/info/warning etc
 * @param context origin of the log message
 * @param msg the actual log message
 * @note if the type is fatal then the program will exit here!
 */
void customMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QString dt = QDateTime::currentDateTimeUtc().toString("yyyy-MM-dd hh:mm:ss");
    QString txt = dt + "\t";
    QString ctx;
#ifdef QT_DEBUG
    ctx = QString("FILE=") + context.file + " LINE=" + QString::number(
                      context.line) + " FUNC=" + context.function;
#else
    (void)context;
    ctx = "";
#endif
    switch (type) {
    case QtDebugMsg: {
        txt += QString("Debug   \t") + msg.simplified() + "\t" + ctx;
        RotateAndLog(debuglogfile, txt);
        return;
    }
        /*
    case QtInfoMsg:
        txt += QString("Info    \t") + msg.simplified() + "\t" + ctx;
        break;
        */
    case QtWarningMsg:
        txt += QString("Warning \t") + msg.simplified() + "\t" + ctx;
        break;
    case QtCriticalMsg:
        txt += QString("Critical\t") + msg.simplified() + "\t" + ctx;
        break;
    case QtFatalMsg:
        txt += QString("Fatal   \t") + msg.simplified() + "\t" + ctx;
        break;
    default:
        // this default gets rid of warning about missing info message
        txt += QString("Warning \t") + msg.simplified() + "\t" + ctx;
        break;
    }
    RotateAndLog(errorlogfile, txt);
    if (type == QtFatalMsg) {
        abort();
    }
}

/** Create a lock file to prevent more than one instance of this program */
void checkAndLock()
{
    pid_file = open(pidname, O_CREAT | O_RDWR, 0666);
    int rc = flock(pid_file, LOCK_EX | LOCK_NB);
    if (rc) {
        if (EWOULDBLOCK == errno) {
            // another instance is running
            qDebug() << "Can not lock the lock";
            qCritical() << "Server already running";
            exit(1);
        }
    } else {
        // this is the first instance
        qDebug() << "Locked the lock";
    }
}

/** main program, set up server and database */
int main(int argc, char *argv[])
{
    qInstallMessageHandler(customMessageHandler);
    checkAndLock();
    QCoreApplication a(argc, argv);
    TcpServer server(&a);
    Storage storage(&a);
    QObject::connect(&server, &TcpServer::messageReady, &storage, &Storage::dataReady);
    return a.exec();
}
