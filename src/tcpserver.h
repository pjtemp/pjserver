/**
  @file tcpserver.h
  @brief Handle a list of sockets and get data
  @author Peter
  @date 2019-10-02
  See this website: https://riptutorial.com/qt/example/29874/tcp-server
*/

#pragma once

#include <QObject>
#include <QDebug>
#include <QDataStream>
#include <QJsonDocument>
#include <QJsonObject>
#include <QList>
#include <QSettings>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QNetworkSession>

class TcpServer : public QObject
{
    Q_OBJECT
public:
    TcpServer(QObject *parent);
    ~TcpServer();
signals:
    /** send a new json object with data package */
    void messageReady(const QJsonObject msg);
public slots:
    /** start a conversation with a new device on a socket */
    void onNewConnection();
    /** clean up if client leaves the conversation */
    void onSocketStateChanged(QAbstractSocket::SocketState socketState);
    /** read data up to RS then emit a json object if it looks good */
    void onReadyRead();
private:
    /** server object */
    QTcpServer _server;
    /** all the current clients */
    QList<QTcpSocket*> _sockets;
    /** for each client keep a buffer */
    QMap<QTcpSocket*, QString> msgbuf;
};
