/**
  @file storage.h
  @brief Connect to database and save data
  @author Peter
  @date 2019-10-06
  When data is received, it is assumed that the data table in the database has
  the same column names as the identifier names in the JSON.
  For example  "unit", "temp", "humid"
  Before the record is stored, another field "timestamp" is added which is
  "seconds since epoch"
  Connection data is read from the config INI file.
*/

#pragma once
#include <QObject>
#include <QDateTime>
#include <QDebug>
#include <QJsonObject>
#include <QSettings>
#include <QtSql>
#include <QtSql/QSqlDatabase>

/** Handle database connection and save new data */
class Storage : public QObject
{
    Q_OBJECT
public:
    explicit Storage(QObject *parent = nullptr);
    ~Storage();
signals:
public slots:
    /** when a message is ready it is received here */
    void dataReady(QJsonObject msg);
private:
    /** database connection */
    QSqlDatabase db;
    /** name of table in db */
    QString table;
};
