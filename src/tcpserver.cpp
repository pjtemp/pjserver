/**
  @file tcpserver.cpp
  @brief Handle a list of sockets and get data
  @author Peter
  @date 2019-10-02
  See this website: https://riptutorial.com/qt/example/29874/tcp-server
*/

#include "tcpserver.h"

const QString inifilename = "/opt/pj/pjserver.ini";
const quint16 defaultport = 4242;
const int bufferlen = 1000;
const char RS = 0x1E;
const char ACK = 0x06;
const char NAK = 0x15;

TcpServer::TcpServer(QObject *parent)
    : QObject(parent)
{
    QSettings settings(inifilename, QSettings::IniFormat);
    settings.beginGroup("network");
    quint16 serverport = settings.value("port", defaultport).toInt() &0xFFFF;
    _server.listen(QHostAddress::Any, serverport);
    connect(&_server, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
}

TcpServer::~TcpServer()
{
    for (auto s: _sockets) {
        s->close();
    }
    _server.close();
}

void TcpServer::onNewConnection()
{
    QTcpSocket *clientSocket = _server.nextPendingConnection();
    connect(clientSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(clientSocket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(onSocketStateChanged(QAbstractSocket::SocketState)));
    _sockets.push_back(clientSocket);
    msgbuf.insert(clientSocket, "");
    qDebug() << clientSocket->peerAddress().toString() + " connected to server !\n";
}

void TcpServer::onSocketStateChanged(QAbstractSocket::SocketState socketState)
{
    if (socketState == QAbstractSocket::UnconnectedState) {
        QTcpSocket* sender = static_cast<QTcpSocket*>(QObject::sender());
        _sockets.removeOne(sender);
        msgbuf.remove(sender);
    }
}

void TcpServer::onReadyRead()
{
    QTcpSocket* sender = static_cast<QTcpSocket*>(QObject::sender());
    char ch = 0;
    QString &buf = msgbuf[sender];
    while (sender->bytesAvailable() > 0) {
        while (ch != RS and buf.length()<bufferlen and sender->bytesAvailable()>0) {
            sender->read(&ch, 1);
            buf.append(ch);
        }
        if (ch == RS) {
            // "normal package was received all is good";
            buf.chop(1);
        } else if (buf.length() == bufferlen) {
            qWarning() << "buffer overrun, dump the string and try again";
            buf.clear();
            return;
        } else {
            qDebug() << "no more data but end of package not reached yet";
            return;
        }
        QJsonObject obj;
        QJsonDocument doc = QJsonDocument::fromJson(buf.toUtf8());
        // check validity of the document
        if (not doc.isNull()) {
            if(doc.isObject()) {
                obj = doc.object();
                // finally there is a json object
                emit messageReady(obj);
                sender->write(&ACK, 1);
            } else {
                qWarning() << "Document is not an object" << buf;
                sender->write(&NAK, 1);
            }
        } else {
            qWarning() << "Invalid JSON..." << buf;
            sender->write(&NAK, 1);
        }
        buf.clear();
    }
}
